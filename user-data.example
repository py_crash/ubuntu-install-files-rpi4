#cloud-config

## These are cloud-init configuration files for ubuntu 20.04.

# Change the configuration of this file before using. Failing to do so will
# result in an unbootable or broken system.

# You will need to change:
# - the default user (Lines 18-28)
# - fstab configuration (Lines 42-55)
# - the hostname (Lines 70-71)

# For the cloud-init documentation visit: https://cloudinit.readthedocs.io/

## User creation
users:

# Create a user named USER. Change USER, REAL NAME, PASSWD and SSH KEYS to the desired values
- name: USER
  gecos: REAL NAME
  groups: [adm, dialout, cdrom, floppy, sudo, audio, dip, video, plugdev, netdev, lxd]
  shell: /bin/bash
  lock_passwd: false
  # Password hash created with: `mkpasswd --method=SHA-512 --rounds=4096`
  passwd: PASSWORD_HASH
  # Add the autorized ssh_keys (public keys)
  ssh_authorized_keys:
    - ssh-ed25519 XXXX USER@HOSTNAME

# If you want to create a user named ubuntu just add `- default` to the list of users

## Update apt database and upgrade packages on first boot
## You will need a wired connection for it.
##
## I won't even bother since the Wi-Fi doesn't work on first boot.
#package_update: true
#package_upgrade: true

## Write arbitrary files to the file-system
write_files:

## Set swappiness to 20. This means that swap will take place less often.
## It won't help perfonce, however preserve our flash storage
- path: /etc/sysctl.d/swappiness.conf
  content: |
    # Set swappiness to 20. It is used to preserve our flash memory drive.
    vm.swappiness=20
  permissions: '0644'
  owner: root:root

## Enable Byobu for our user
byobu_by_default: user

## Change the server hostname
# Replace HOSTNAME with the desired hostname
hostname: HOSTNAME

## Run arbitrary commands
runcmd:
## Create the mount points for the external disks
- [mkdir, /mnt/storage]

## Reboot after cloud-init completes because otherwise wlan0 will not come up...
## despite cloud-init having applied settings from network-config
## Thanks DavidUnboxed for the fix: https://github.com/DavidUnboxed/Ubuntu-20.04-WiFi-RaspberyPi4B
power_state:
  mode: reboot
