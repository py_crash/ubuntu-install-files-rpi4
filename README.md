# Ubuntu 20.04 on Raspberry Pi: Install Files

These are examples files for installing Ubuntu on the Raspberry Pi. If you want to
use them, just modify them and copy them into the `system-boot` partition of the SD Card
(exept for `fstab` that goes into the `/etc/` from the root filesystem).

## cmdline and fstab

The `cmdline` and the `fstab.example` files are needed for the installation of Ubuntu 20.04 using BTRFS on root.
If you want to know more on how to use it, check this [article] on my [blog].

## usercfg.txt

The `usercfg.txt` file is needed for activating hardware acceleration on the raspberry pi 4. This is useful, for example,
when running Jellyfin.

## network-config and user-data

The `user-data` file allows you to configure your system to your liking. You can _install packages_ and/or
_update your system_ (asuming you have networking running on first boot); _create folders and arbitrary files_;
configure your _user account_, including passwords, ssh-keys, and other things; _set your hostname_;
_run arbitrary commands_; etc.

The `network-config` file, as the name suggests, configures networking: both ethernet and Wi-Fi. In Ubuntu 20.04
the changes are picked after the first reboot, so a reboot is added into `user-data` to allow me to use Wi-Fi.

Please remmember that this are YAML files (altough they are saved without extension on `system-boot`). This means
that spaces are important. More information could be found in the [Documentation]. An article will come on my blog.

<!-- Links -->
[article]: https://ferrario.me/ubuntu-on-the-raspberry-pi-part-i-installing-ubuntu-20.04-with-btrfs-on-root/
[blog]: https://ferrario.me/
[Documentation]: https://cloudinit.readthedocs.io/
